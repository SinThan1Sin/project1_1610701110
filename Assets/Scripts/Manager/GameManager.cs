﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
   public class GameManager : MonoSingleton<GameManager>
   {
      public static GameManager Instance { get; private set; }


      [SerializeField] private RectTransform dialog;
      [SerializeField] private PlayerSpaceship playerSpaceship;
      [SerializeField] private EnemySpaceship enemySpaceship;
      [SerializeField] private AudioSource audioSource;
      [SerializeField] private int playerSpaceshipHp;
      [SerializeField] private int playerSpaceshipMoveSpeed;
      [SerializeField] private int enemySpaceshipHp;
      [SerializeField] private int enemySpaceshipMoveSpeed;

      [SerializeField] private Bullet bullet;

      public int currentScene = 1;

      public event Action OnRestarted;

      private void Awake()
      {
         Debug.Assert(dialog != null, "dialog cannot be null");
         Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
         Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
         Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
         Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
         Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
         Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

         if (Instance == null)
         {
            Instance = this;
         }

         DontDestroyOnLoad(this);

      }

      public void Start()
      {
         SceneManager.sceneLoaded += OnSceneLoaded;
      }

      void OnSceneLoaded(Scene scene, LoadSceneMode mode)
      {
         if (currentScene == 2)
         {
            dialog = GameObject.FindWithTag("Panel").GetComponent<RectTransform>();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            dialog.gameObject.SetActive(false);
            SoundManager.Instance.PlayNextBGM();
         }
      }

      public void OnStartButtonClicked()
      {
         dialog.gameObject.SetActive(false);
         StartGame();
         SoundManager.Instance.PlayBGM();
      }

      public void StartGame()
      {
         ScoreManager.Instance.Init(this);
         SpawnPlayerSpaceship();
         SpawnEnemySpaceship();
      }

      private void SpawnPlayerSpaceship()
      {
         var spaceship = Instantiate(playerSpaceship);
         spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed, bullet);
         spaceship.OnExploded += OnPlayerSpaceshipExploded;
      }

      private void OnPlayerSpaceshipExploded()
      {
         SoundManager.Instance.Play(audioSource, SoundManager.Sound.Die);
         UIManager.Instance.OnRestartGame();
      }

      private void SpawnEnemySpaceship()
      {
         var spaceship = Instantiate(enemySpaceship);
         spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed, bullet);
         spaceship.OnExploded += OnEnemySpaceshipExploded;
      }

      private void OnEnemySpaceshipExploded()
      {
         var currentScore = ScoreManager.Instance.Score;
         ScoreManager.Instance.SetScore(currentScore++);
         SoundManager.Instance.Play(audioSource, SoundManager.Sound.Die);
         if (currentScene == 1)
         {
            SceneManager.LoadSceneAsync("SecondScene");
            currentScene = 2;
         }
         else if (currentScene == 2)
         {
            SceneManager.LoadSceneAsync("FirstScene");
            currentScene = 1;
         }


      }

      private void Restart()
      {
         dialog.gameObject.SetActive(true);
         OnRestarted?.Invoke();
      }

      public void NextLV()
      {
         StartGame();
         SoundManager.Instance.PlayNextBGM();
         UIManager.Instance.OnCloseGoNextLevelDialog();
      }

   }
}
