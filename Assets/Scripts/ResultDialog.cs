﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Manager
{
   public class ResultDialog : MonoBehaviour
   {
      [SerializeField] private Button restartButton;
      [SerializeField] private TextMeshProUGUI highScoreText;

      public void Init()
      {
         SetHighScoreText(ScoreManager.Instance.Score);
      }

      private void Awake()
      {
         Debug.Assert(restartButton != null, "restartButton cannot be null");
         Debug.Assert(highScoreText != null, "highScoreText cannot be null");

         restartButton.onClick.AddListener(OnRestartButtonClicked);
      }

      private void SetHighScoreText(int score)
      {
         highScoreText.text = $"High Score : {score}";
      }

      private void OnRestartButtonClicked()
      {
         gameObject.SetActive(false);

         GameManager.Instance.StartGame();
      }
   }
}
